package kalkulaator;

import java.util.Arrays;

public class OnePlusOne
{
	public static int onePlusOne()
	{
		return 1 + 1;
	}

	public static int sumEvenNumbers(int[] numbers){
		return Arrays.stream(numbers).filter(n -> n%2 == 0).filter(n -> n>=0).sum();
	}

	public static int[] sortNumbers(int[] numbers){
		return Arrays.stream(numbers).sorted().toArray();
	}
}
