package kalkulaatortest;

import static org.junit.Assert.*;

import org.junit.Test;

import kalkulaator.OnePlusOne;

public class OnePlusOneTest
{
	@Test
	public void test()
	{
		assertEquals("Result is different then expected", 2, OnePlusOne.onePlusOne());
	}

	@Test
	public void testEvenNumbersSum(){
		assertEquals(2, OnePlusOne.sumEvenNumbers(new int[]{2}));
		assertEquals(12, OnePlusOne.sumEvenNumbers(new int[]{2, 4, 6}));
		assertEquals(12, OnePlusOne.sumEvenNumbers(new int[]{-1, -4, 2, 4, 6}));
		assertEquals(12, OnePlusOne.sumEvenNumbers(new int[]{1, 3, 2, 4, 6}));
		assertEquals(12, OnePlusOne.sumEvenNumbers(new int[]{-2, -4, 2, 4, 6}));
	}

	@Test
	public void testSorting(){
		assertArrayEquals(new int[]{}, OnePlusOne.sortNumbers(new int[]{}));
		assertArrayEquals(new int[]{1}, OnePlusOne.sortNumbers(new int[]{1}));
		assertArrayEquals(new int[]{1,2}, OnePlusOne.sortNumbers(new int[]{2,1}));
		assertArrayEquals(new int[]{1,2}, OnePlusOne.sortNumbers(new int[]{1,2}));
		assertArrayEquals(new int[]{1,2,3}, OnePlusOne.sortNumbers(new int[]{3,1,2}));
		assertArrayEquals(new int[]{1,2,3}, OnePlusOne.sortNumbers(new int[]{3,2,1}));
		assertArrayEquals(new int[]{1,2,3}, OnePlusOne.sortNumbers(new int[]{1,3,2}));
		assertArrayEquals(new int[]{1,2,3}, OnePlusOne.sortNumbers(new int[]{1,2,3}));
	}
}
